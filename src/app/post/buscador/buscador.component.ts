import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PostService } from '../post.service';
import { IPost } from '../ipost';

@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html',
  styleUrls: ['./buscador.component.styl']
})
export class BuscadorComponent implements OnInit {

  buscadorFormGroup: FormGroup;
  
  post: IPost[];

  statusSearchPostId: boolean = false;

  formSearchReto = this.formBuilder.group({
    postId: ['']
  });

  constructor(private formBuilder: FormBuilder, private postService: PostService ) { }

  ngOnInit() {
  }

  searchPostId(){
    console.warn('Data', this.formSearchReto.value);
    this.postService.getRetoByPostId(this.formSearchReto.value.postId)
    .subscribe(res => {
      console.warn('Response reto by postId', res);
      this.post = res;
      if(res.length > 0){
        
        this.statusSearchPostId = false;
      }else{
        this.statusSearchPostId = true;
      }
  
    });

  }

}
