export interface IPost {
    postId: String;
    id: number;
    name: String;
    email: String;
    body: String
}
