import { Component, OnInit } from '@angular/core';
import { IPost } from '../ipost';
import { PostService } from '../post.service';

@Component({
  selector: 'app-buscador-list',
  templateUrl: './buscador-list.component.html',
  styleUrls: ['./buscador-list.component.styl']
})
export class BuscadorListComponent implements OnInit {

public buscadorList: IPost[];

  constructor(private buscadorService: PostService) { }

  ngOnInit() {
    this.buscadorService.query()
    .subscribe(res => {
      console.log('response data', res);
      return this.buscadorList = res;
    }, error => console.error('error')
    );
  }

}
