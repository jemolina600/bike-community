import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IPost } from './ipost';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http: HttpClient) { }


public query(): Observable<IPost[]>{
  return this.http.get<IPost[]>(`${environment.ENN_POINT}/comments`)
  .pipe(map(res => {
    return res;
  }, error => {
    console.error('error', error)
  }));
}

  public getRetoByPostId(postId: string): Observable<IPost[]>{
    let params = new HttpParams();
    params = params.append('postId', postId);
    console.warn('PARAMS', params);
    return this.http.get<IPost[]>(`${environment.ENN_POINT}/comments`,{params: params})
    .pipe(map(res => {
      return res;
    }));
  }

}
