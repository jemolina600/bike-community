import { Component, OnInit } from '@angular/core';
import { Iuser } from '../iuser';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.styl']
})
export class UserListComponent implements OnInit {

public userList: Iuser[];

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.query()
    .subscribe(res => {
      console.log('response data', res);
      return this.userList = res;
    }, error => console.error('error')
    );
  }

}
