export interface Sale {

    id?: Number;
    date?: Date;
    clientId?: Number;
    clientName?: String;
    bikesId?: Number;
    bikeSerial?: String;
    bikesPrice?: number


}

