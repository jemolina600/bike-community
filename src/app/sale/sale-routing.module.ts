import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SaleCreateComponent } from './sale-create/sale-create.component';
import { SaleListComponent } from './sale-list/sale-list.component';
import { MainComponent } from './main/main.component';


const routes: Routes = [
 {
   path: '',
   component: MainComponent,
   children: [
    {
      path: 'sale-create',
      component: SaleCreateComponent
  },
  {
    path: 'sale-list',
    component: SaleListComponent
  }
   ]
 }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SaleRoutingModule { }
