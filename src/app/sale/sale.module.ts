import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SaleCreateComponent } from './sale-create/sale-create.component';
import { SaleRoutingModule } from './sale-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { SaleListComponent } from './sale-list/sale-list.component';
import {  HttpClientModule } from '@angular/common/http';
import { MainComponent } from './main/main.component';



@NgModule({
  declarations: [SaleCreateComponent,  SaleListComponent, MainComponent],
  imports: [
    CommonModule,
    SaleRoutingModule ,
    ReactiveFormsModule,
    HttpClientModule
  ]
})
export class SaleModule { }
