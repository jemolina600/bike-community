import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Sale } from './sale';
import { Bike } from '../components/bikes/interfaces/bike';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Interface } from '../clients/interface';

@Injectable({
  providedIn: 'root'
})
export class SaleService {

  constructor(private http: HttpClient) { }

  public query(): Observable<Sale[]>{
    return this.http.get<Sale[]>(`${environment.ENN_POINT1}/api/sales`)
    .pipe(map(res => {
      return res;
    }));
  }

  public getClientDocument(document: string): Observable<Interface[]> {
    let params = new HttpParams();
    params = params.append('document', document);
    console.warn('document ok', document);
    return this.http.get<Interface[]>(`${environment.ENN_POINT1}/api/clients`, {params: params})
    .pipe(map( res => {
      return res;
    }, error => console.error('search faile', error)));
  }

}






