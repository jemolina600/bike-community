import { Component, OnInit } from '@angular/core';
import { Bike } from 'src/app/components/bikes/interfaces/bike';
import { Sale } from '../sale';
import { BikesService } from 'src/app/components/bikes/bikes.service';
import { Interface } from 'src/app/clients/interface';
import { ClientsService } from 'src/app/clients/clients.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SaleService } from '../sale.service';


@Component({
  selector: 'app-sale-create',
  templateUrl: './sale-create.component.html',
  styleUrls: ['./sale-create.component.styl']
})
export class SaleCreateComponent implements OnInit {

searchFormGroup: FormGroup;

saleList: Bike[];

saleListCar: Sale[] = [];

clientList: Interface[];

suma: number = 0;

clientSeletedTemp: Interface;

clientSearchTemp: Interface;

statusSearchClientDocument: boolean = false;

formSearchClient = this.formBuilder.group({
  document: ['']
})


constructor (
  private saleService: BikesService,
  private clientServe: ClientsService,
  private salesService: SaleService,
  private formBuilder: FormBuilder
  )
   {

   }

  ngOnInit() {
    this.saleService.query()
    .subscribe( res => {
      this.saleList = res;
      console.log('Response Data', res);
    }, error => console.error('Error', error));


      this.clientServe.queryClients()
      .subscribe(res => {
        this.clientList = res;
      }, error => console.error('error', error))


  }

  addToCar(bike: Bike): void{
    console.warn('Seleted', bike);
    this.suma += bike.price;
    this.saleListCar.push({
      clientId:this.clientSeletedTemp.id,
      bikeSerial: bike.serial,
      bikesPrice: bike.price
    })

    }

    seletedClient(client: Interface): void{
        this.clientSeletedTemp = client
        console.warn('seleted client ok',this.clientSeletedTemp)
    }
/*
    searchClient(clients: Interface): void {
      this.clientSearchTemp = clients
      console.warn('Seletec clients ok', clients)
    }
*/
    searchClientDocument(){
      console.warn('Data', this.formSearchClient.value);
      this.salesService.getClientDocument(this.formSearchClient.value.document)
      .subscribe( res => {
        if (res.length > 0){
          this.statusSearchClientDocument = false
        } else {
          this.statusSearchClientDocument = true
        }
      });
    }

    searchClient(): void {
      console.warn('keys up');
      this.clientServe.queryClients({
        'document.contains': this.formSearchClient.value.document
      })
      .subscribe(res => {
        this.clientList = res;
      })
    }








}
