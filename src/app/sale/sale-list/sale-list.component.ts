import { Component, OnInit } from '@angular/core';
import { Sale } from '../sale';
import { SaleService } from '../sale.service';

@Component({
  selector: 'app-sale-list',
  templateUrl: './sale-list.component.html',
  styleUrls: ['./sale-list.component.styl']
})
export class SaleListComponent implements OnInit {

public salesList: Sale[];

  constructor(private saleService: SaleService) { }

  ngOnInit() {
    this.saleService.query()
    .subscribe(res => {
      this.salesList = res;
      console.log('Response Data', res);
    }, error =>{
      console.error('Error', error)
    });

  }

}
