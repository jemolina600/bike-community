import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'bikes',
    loadChildren: () => import('./components/bikes/bikes.module')
    .then(modulo => modulo.BikesModule)
  },
  {
    path: 'clients',
    loadChildren: () => import('./../app/clients/clients.module')
    .then(modulo => modulo.ClientsModule)
  },
  {
    path: 'sale',
    loadChildren: () => import('./sale/sale.module')
    .then(modulo => modulo.SaleModule)
  },
  {
    path: 'post',
    loadChildren: () => import('./post/post.module')
    .then(mod => mod.RetoModule)
  },
  {
    path: 'user',
    loadChildren: () => import('./user/user.module')
    .then(mod => mod.UserModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
