export interface Interface {
    id: number;
    name: String;
    document: string;
    email: String;
    phoneNumber: String;
    documentType: String;
}
