import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientsListComponent } from './clients-list/clients-list.component';
import { ClientsFormsComponent } from './clients-forms/clients-forms.component';
import { ClientsUpdateComponent } from './clients-update/clients-update.component';


const routes: Routes = [
  {
    path: '',
    component: ClientsListComponent
  },
  {
    path: 'clients-list',
    component: ClientsListComponent
  },
  {
    path: 'clients-forms',
    component: ClientsFormsComponent
  },
  {
    path: 'clients-update',
    component: ClientsUpdateComponent
  },
  {
    path: 'clients-update/:id',
    component: ClientsUpdateComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientsRoutingModule { }
