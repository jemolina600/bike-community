import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Interface } from './interface';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Bike } from '../components/bikes/interfaces/bike';
import { createRequestParams } from '../utils/request.utils';

@Injectable({
  providedIn: 'root'
})
export class ClientsService {

  constructor( private http: HttpClient ) { }


  public queryClients(req?: any): Observable <Interface[]> {
    let params = createRequestParams(req);
    return this.http.get<Interface[]>(`${environment.ENN_POINT1}/api/clients`,{params: params})
    .pipe(map(res => {
      return res;
    }));
  }

  public saveClient(clients: Interface): Observable<Interface> {
    return this.http.post<Interface>(`${ environment.ENN_POINT1 }/api/clients`, clients)
    .pipe(map(res => {
      return res;
    }));
    }

public getClientsById(id: string){
  return this.http.get<Interface>(`${ environment.ENN_POINT1 }/api/clients/${id}`)
    .pipe(map(res => {
      return res;
    }));
}

public updateClient(clients: Interface): Observable<Interface>{
  return this.http.put<Interface>(`${ environment.ENN_POINT1 }/api/clients`,clients)
  .pipe(map(res => {
    return res;
  }));
}

public deletItem(id: string): Observable<Bike>{
  return this.http.delete<Bike>(`${environment.ENN_POINT1}/api/clients/${id}`)
  .pipe(map(res => {
    return res;
  }));
}

}

