import { Component, OnInit } from '@angular/core';
import { ClientsService } from './../clients.service'
import { Interface } from '../interface';

@Component({
  selector: 'app-clients-list',
  templateUrl: './clients-list.component.html',
  styleUrls: ['./clients-list.component.styl']
})
export class ClientsListComponent implements OnInit {

  public clientsList: Interface[];

  constructor(private clientsService: ClientsService ) { }

  ngOnInit() {
    this.clientsService.queryClients()
    .subscribe(res => {
      this.clientsList = res;
      console.log('response data', res);
    },
    error => console.error('error',error)
    );
  }

  saveClients(){
    console.log('Datos',)
  }

  deleteItem(id: string){
    console.warn('id', id);
    this.clientsService.deletItem(id)
    .subscribe(res => {
      console.warn('Item delete ok');
      return res;
    }, error => console.log('Error', error))
  }

}
