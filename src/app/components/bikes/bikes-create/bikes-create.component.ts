import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BikesService } from '../bikes.service';

@Component({
  selector: 'app-bikes-create',
  templateUrl: './bikes-create.component.html',
  styleUrls: ['./bikes-create.component.styl']
})
export class BikesCreateComponent implements OnInit {

  bikeFormGroup: FormGroup;

  constructor( private formBuilder: FormBuilder, private bikeService:BikesService ) { 

    this.bikeFormGroup = this.formBuilder.group({
      model:  ['', Validators.compose([ Validators.required,Validators.maxLength(4)])],
      price:  ['',Validators.compose([ Validators.required,Validators.maxLength(4)])],
      serial: ['',Validators.compose([ Validators.required,Validators.maxLength(4)])]

    })

   }

  ngOnInit() {
  }

saveBike(){
  console.log('Datos', this.bikeFormGroup.value);
  this.bikeService.saveBike(this.bikeFormGroup.value)
  .subscribe(res => {
    console.log('Save ok', res);
  }, error => {
console.error('Error', error);
  });
}

}
