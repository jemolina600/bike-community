import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductSalesComponent } from './product-sales/product-sales.component';



@NgModule({
  declarations: [ProductSalesComponent],
  imports: [
    CommonModule
  ]
})
export class AdminModule { }
